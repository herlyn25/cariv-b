
import { Routes, Route} from "react-router-dom";

function Home () {
  return <h1> home </h1>
}
function About () {
  return <h1> about</h1>
}

function App() {
  return (
    <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
        </Routes>
    </div>
  )
}

export default App
